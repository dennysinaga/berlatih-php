<?php
function tukar_besar_kecil($string){
    $old_string = $string;
    $length = strlen($old_string);


    for ($i = 0; $i < $length; $i++) {
        $string_validation = substr($old_string, $i, 1);
        if (ctype_upper($string_validation) == true) {
            $new_string = strtolower($string_validation);
        } else {
            $new_string = strtoupper($string_validation);
        }
        str_replace($string_validation, $new_string, $old_string);
        echo $new_string;
    }
    echo "<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>